/*******************************************************************************
 * Copyright (c) 2006 Synchrotron SOLEIL
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.All rights reserved. This program and the accompanying materials
 * 
 * Contributors: katy.saintin@synchrotron-soleil.fr - initial implementation
 *******************************************************************************/

package fr.soleil.bean.generic;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.prefs.Preferences;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;

import org.tango.utils.TangoUtil;

import fr.esrf.Tango.DevFailed;
import fr.soleil.NI6602;
import fr.soleil.bean.motor.UserMotorBean;
import fr.soleil.bean.tangoparser.TangoParserBean;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.complexbox.ChartViewerBox;
import fr.soleil.comete.box.matrixbox.BooleanMatrixBox;
import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.box.scalarbox.BooleanScalarBox;
import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.swing.BooleanComboBox;
import fr.soleil.comete.swing.BooleanMatrixTable;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.CheckBox;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.TextArea;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.comete.swing.chart.data.DataView;
import fr.soleil.comete.swing.chart.data.DataViewList;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.tango.data.service.TangoDataSourceFactory;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.helper.TangoAttributeHelper;
import fr.soleil.comete.tango.data.service.helper.TangoConstHelper;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.file.BatchExecutor;

/**
 * @author saintin
 */
public class CommonDeviceAttributeBean extends AbstractTangoBox {

    private static final long serialVersionUID = 4880908980892231516L;

    private static final String NOT_CONNECTED = "Not connected";
    private static final String CONTROL_PANEL = "Control Panel";
    private static final String CONNECTION_ERROR_SYFFIX = " connection error!";

    private static final String XPS_AXIS = "xpsaxis";
    private static final String GALIL_AXIS = "galilaxis";
    private static final String PSEUDO_AXIS = "pseudoaxis";
    private static final String TANGO_PARSER = "tangoparser";
    private static final String NI6602 = "ni6602";

    private static final String DEVICE_EXPR = "{DEVICE}";

    private final BatchExecutor batchExecutor;

    /**
     * Used for number scalar attribute.
     */
    private Label readNumberField;
    private TextField textField;
    private TextArea textArea;
    private CheckBox checkBox;
    private BooleanComboBox booleanComboBox;

    /**
     * Used for writable number scalar attribute.
     */
    private WheelSwitch wheelswitch;
    private final Component glue;
    /**
     * Used for spectrum attributes.
     */
    private Chart chartViewer;
    /**
     * Used for image attribute.
     */
    private ImageViewer imageViewer;

    private BooleanMatrixTable booleanTable;
    /**
     * The button that launches the control panel.
     */
    private JButton controlPanelButton;
    /**
     * A UserMotorBean
     */
    private UserMotorBean motorBean;
    /**
     * A TangoParserBean
     */
    private TangoParserBean tangoParserBean;
    /**
     * A TangoParserBean
     */
    private NI6602 timeBaseBean;
    /**
     * Title Border
     */
    private final TitledBorder titleBorder;

    private final NumberScalarBox numberScalarBox;
    private final BooleanScalarBox booleanScalarBox;
    private final BooleanMatrixBox booleanMatrixBox;
    private final ChartViewerBox chartBox;
    private final NumberMatrixBox imageBox;

    private ImageProperties imageProperties;
    private ChartProperties chartProperties;
    private ChartProperties defaultChartProperties;

    private final Map<String, PlotProperties> plotPropertiesMap;

    /**
     * AskConfirmation
     */
    private boolean confirmation;

    /**
     * The name of the attribute
     */
    private String attributeName;

    /**
     * isAMotorClass
     */
    private boolean aMotorClass;
    /**
     * isATimeBaseClass
     */
    private boolean aTimeBaseClass;
    /**
     * isATangoParserClass
     */
    private boolean aTangoParserClass;
    /**
     * FactoryClassName
     */

    /**
     * The batch executed when clicking on control panel
     */
    private String executedBatchFile;

    private boolean modelIsSet;

    /**
     * The panel containing the top elements.
     */
    private final JPanel defaultPanel;
    private final JPanel complexPanel;

    public CommonDeviceAttributeBean() {
        super();
        glue = Box.createGlue();
        batchExecutor = new BatchExecutor();
        booleanMatrixBox = new BooleanMatrixBox();
        titleBorder = new TitledBorder(NOT_CONNECTED);
        numberScalarBox = new NumberScalarBox();
        booleanScalarBox = new BooleanScalarBox();
        chartBox = new ChartViewerBox();
        imageBox = new NumberMatrixBox();
        imageProperties = null;
        chartProperties = null;
        plotPropertiesMap = new HashMap<>();
        confirmation = false;
        attributeName = null;
        aMotorClass = false;
        aTimeBaseClass = false;
        aTangoParserClass = false;
        executedBatchFile = null;
        modelIsSet = false;
        defaultPanel = new JPanel(new FlowLayout());
        complexPanel = new JPanel(new FlowLayout());

        initGUI();
    }

    private void initGUI() {
        setBorder(titleBorder);
        setLayout(new BorderLayout());
        add(defaultPanel, BorderLayout.NORTH);
        add(complexPanel, BorderLayout.CENTER);
    }

    private void initReadNumberField() {
        if (readNumberField == null) {
            readNumberField = generateLabel();
        }
    }

    private void initTextField() {
        if (textField == null) {
            textField = generateTextField();
        }
    }

    private void initTextArea() {
        if (textArea == null) {
            textArea = new TextArea();
        }
    }

    private void initCheckBox() {
        if (checkBox == null) {
            checkBox = new CheckBox();
        }
    }

    private void initBooleanComboBox() {
        if (booleanComboBox == null) {
            booleanComboBox = new BooleanComboBox();
        }
    }

    protected void initWheelSwitch() {
        if (wheelswitch == null) {
            wheelswitch = generateWheelSwitch();
            wheelswitch.setInvertionLogic(true);
            if (confirmation) {
                numberScalarBox.setConfirmation(wheelswitch, confirmation);
            }
        }
    }

    private void initChartViewer() {
        if (chartViewer == null) {
            chartViewer = new Chart();
            chartViewer.setFreezePanelVisible(true);
            chartViewer.setManagementPanelVisible(false);
            chartViewer.setAutoHighlightOnLegend(true);
            chartViewer.setCyclingCustomMap(true);
            chartViewer.setCustomCometeColor(new CometeColor[] { CometeColor.CYAN, CometeColor.MAGENTA,
                    CometeColor.GREEN, CometeColor.ORANGE, CometeColor.PURPLE, CometeColor.DARK_GRAY });
            chartViewer.setCleanDataViewConfigurationOnRemoving(false);
            defaultChartProperties = chartViewer.getChartProperties();
            if (confirmation) {
                chartBox.setConfirmation(chartViewer, confirmation);
            }
        }
    }

    private void initImageViewer() {
        if (imageViewer == null) {
            imageViewer = new ImageViewer();
            imageViewer.setApplicationId(CommonDeviceAttributeBean.class.getName());
            imageViewer.setAlwaysFitMaxSize(true);
            if (confirmation) {
                imageBox.setConfirmation(imageViewer, confirmation);
            }
        }
    }

    private void initBooleanTable() {
        if (booleanTable == null) {
            booleanTable = new BooleanMatrixTable();
        }
    }

    private void initControlPanelButton() {
        if (controlPanelButton == null) {
            controlPanelButton = new JButton();
            controlPanelButton.setText(CONTROL_PANEL);
            controlPanelButton.addActionListener((e) -> {
                controlPanelActionPerformed();
            });
        }
    }

    private void initMotorBean() {
        if (motorBean == null) {
            motorBean = new UserMotorBean();
            if ((executedBatchFile != null) && (!executedBatchFile.isEmpty())) {
                motorBean.setExecutedBatchFile(executedBatchFile);
            }
            if (attributeName != null) {
                motorBean.setAttributeName(attributeName);
            }
        }
    }

    private void initTangoParserBean() {
        if (tangoParserBean == null) {
            tangoParserBean = new TangoParserBean();
            if (attributeName != null) {
                tangoParserBean.setAttributeName(attributeName);
            }
        }
    }

    private void initTimeBaseBean() {
        if (timeBaseBean == null) {
            timeBaseBean = new NI6602();
        }
    }

    private void updatePanel(JPanel panel, Component... components) {
        panel.removeAll();
        for (Component component : components) {
            if (component != null) {
                panel.add(component);
            }
        }
    }

    private void setVisible(boolean visible, Component... components) {
        for (Component component : components) {
            if (component != null) {
                component.setVisible(visible);
            }
        }
    }

    private void updateDefaultPanel() {
        updatePanel(defaultPanel, readNumberField, checkBox, wheelswitch, textField, booleanComboBox, getStateLabel(),
                controlPanelButton);
    }

    private void updateComplexPanel() {
        updatePanel(complexPanel, glue, chartViewer, imageViewer, motorBean, tangoParserBean, timeBaseBean, textArea,
                booleanTable);
    }

    public String getControlButtonText() {
        initControlPanelButton();
        return controlPanelButton.getText();
    }

    public boolean isConfirmation() {
        return confirmation;
    }

    public void setConfirmation(boolean confirmation) {
        this.confirmation = confirmation;
        numberScalarBox.setConfirmation(wheelswitch, confirmation);
        chartBox.setConfirmation(chartViewer, confirmation);
        imageBox.setConfirmation(imageViewer, confirmation);
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
        computeCompleteAttributeName();
    }

    public String getCompleteAttributeName() {
        String completeAttributeName = null;
        if (model != null) {
            completeAttributeName = model;
        }
        if (attributeName != null) {
            completeAttributeName = completeAttributeName + TangoDeviceHelper.SLASH + attributeName;
        }
        return completeAttributeName;
    }

    public void setCompleteAttributeName(String acompleteAttributeName) {
        modelIsSet = false;
        setModel(null);
        titleBorder.setTitle(NOT_CONNECTED);
        titleBorder.setTitleColor(Color.BLACK);
        if (acompleteAttributeName != null) {
            titleBorder.setTitle(acompleteAttributeName);
            String[] nbTokens = acompleteAttributeName.split(TangoDeviceHelper.SLASH);
            if (nbTokens.length > 3) {
                model = TangoDeviceHelper.getDeviceName(acompleteAttributeName).toLowerCase();
                attributeName = TangoDeviceHelper.getEntityName(acompleteAttributeName);
                if (motorBean != null) {
                    motorBean.setAttributeName(attributeName);
                }
                if (tangoParserBean != null) {
                    tangoParserBean.setAttributeName(attributeName);
                }
            } else if (nbTokens.length == 3) {
                model = acompleteAttributeName;
                attributeName = null;
                if (motorBean != null) {
                    motorBean.setAttributeName(attributeName);
                }
                if (tangoParserBean != null) {
                    tangoParserBean.setAttributeName(attributeName);
                }
            } else if (nbTokens.length == 2) {
                try {
                    model = TangoUtil.getfullNameForDevice(nbTokens[0]);
                } catch (DevFailed e) {
                    model = null;
                }
                attributeName = nbTokens[1];
                if (motorBean != null) {
                    motorBean.setAttributeName(attributeName);
                }
                if (tangoParserBean != null) {
                    tangoParserBean.setAttributeName(attributeName);
                }
            } else if (nbTokens.length == 1) {
                // It can be a alias of a device or of an attribute
                // Test alias attribute
                String fullattributeName = null;
                try {
                    fullattributeName = TangoUtil.getfullAttributeNameForAttribute(acompleteAttributeName);
                    if (fullattributeName != null) {
                        model = TangoDeviceHelper.getDeviceName(fullattributeName).toLowerCase();
                        attributeName = TangoDeviceHelper.getEntityName(fullattributeName);
                        if (motorBean != null) {
                            motorBean.setAttributeName(attributeName);
                        }
                        if (tangoParserBean != null) {
                            tangoParserBean.setAttributeName(attributeName);
                        }
                    }
                } catch (DevFailed e) {
                    fullattributeName = null;
                }
                if (fullattributeName == null) {
                    try {
                        fullattributeName = TangoUtil.getfullNameForDevice(acompleteAttributeName);
                        if (fullattributeName != null) {
                            model = fullattributeName;
                            attributeName = null;
                            if (motorBean != null) {
                                motorBean.setAttributeName(attributeName);
                            }
                            if (tangoParserBean != null) {
                                tangoParserBean.setAttributeName(attributeName);
                            }
                        }
                    } catch (DevFailed e) {
                    }
                }
            }
            computeCompleteAttributeName();
        }
    }

    private String getDeviceNameClass() {
        return TangoDeviceHelper.getDeviceClass(model);
    }

    private int getAttributeType() {
        return TangoAttributeHelper.getAttributeType(model, attributeName);
    }

    public void computeCompleteAttributeName() {
        if ((model != null) && !model.isEmpty()) {
            String className = getDeviceNameClass();
            setClassType(className);
            if ((attributeName != null) && !attributeName.isEmpty()) {
                titleBorder.setTitle(model + TangoDeviceHelper.SLASH + attributeName);
            } else {
                titleBorder.setTitle(model);
                if (readNumberField != null) {
                    readNumberField.setVisible(false);
                }
                if (wheelswitch != null) {
                    wheelswitch.setVisible(false);
                }
            }
            setModel(model.toLowerCase());
            repaint();
            validate();
        } else {
            clearGUI();
            hideAll();
            String currentTitle = titleBorder.getTitle();
            titleBorder.setTitle(currentTitle + CONNECTION_ERROR_SYFFIX);
            titleBorder.setTitleColor(Color.RED);
        }
        modelIsSet = true;
        start();
    }

    private void setClassType(String className) {
        aMotorClass = false;
        aTimeBaseClass = false;
        aTangoParserClass = false;

        if (motorBean != null) {
            motorBean.setModel(null);
        }
        if (tangoParserBean != null) {
            tangoParserBean.setModel(null);
        }
        if (timeBaseBean != null) {
            timeBaseBean.setModel(null);
        }

        if ((className != null) && !className.isEmpty()) {
            String classNameLower = className.toLowerCase();
            switch (classNameLower) {
                case XPS_AXIS:
                case GALIL_AXIS:
                case PSEUDO_AXIS:
                    initMotorBean();
                    aMotorClass = true;
                    motorBean.setModel(model);
                    EDTManager.INSTANCE.runInDrawingThread(() -> {
                        updateComplexPanel();
                    });
                    break;
                case TANGO_PARSER:
                    initTangoParserBean();
                    aTangoParserClass = true;
                    tangoParserBean.setModel(model);
                    EDTManager.INSTANCE.runInDrawingThread(() -> {
                        updateComplexPanel();
                    });
                    break;
                case NI6602:
                    initTimeBaseBean();
                    aTimeBaseClass = true;
                    timeBaseBean.setModel(model);
                    EDTManager.INSTANCE.runInDrawingThread(() -> {
                        updateComplexPanel();
                    });
                    break;
                default:
                    break;
            }
        }
    }

    private boolean isAMotorClass() {
        return aMotorClass;
    }

    private boolean isATimeBaseClass() {
        return aTimeBaseClass;
    }

    private boolean isATangoParserClass() {
        return aTangoParserClass;
    }

    public String getExecutedBatchFile() {
        return executedBatchFile;
    }

    public void setExecutedBatchFile(String executedBatchFile) {
        this.executedBatchFile = executedBatchFile;
        if (executedBatchFile != null) {
            if (executedBatchFile.endsWith(DEVICE_EXPR)) {
                executedBatchFile = executedBatchFile.substring(0, executedBatchFile.lastIndexOf(DEVICE_EXPR));
            }
            if (!executedBatchFile.isEmpty()) {
                batchExecutor.setBatch(executedBatchFile);
                if (motorBean != null) {
                    motorBean.setExecutedBatchFile(executedBatchFile);
                }
            }
        }
        updateControlPanelButtonVisibility();

    }

    private void updateControlPanelButtonVisibility() {
        boolean visible = !isAMotorClass() && (executedBatchFile != null) && !executedBatchFile.isEmpty()
                && (model != null) && !model.isEmpty();
        if (visible) {
            initControlPanelButton();
        }
        if ((controlPanelButton != null) && (controlPanelButton.isVisible() != visible)) {
            controlPanelButton.setVisible(visible);
        }
    }

    /**
     * When control Button panel is actionned
     */
    public void controlPanelActionPerformed() {
        if (model != null) {
            List<String> parameter = batchExecutor.getBatchParameters();
            if (parameter == null) {
                parameter = new ArrayList<String>();
            }
            parameter.clear();
            parameter.add(model);
            batchExecutor.setBatchParameters(parameter);
            batchExecutor.execute();
        }
    }

    private void hideAll() {
        setVisible(false, readNumberField, chartViewer, imageViewer, motorBean, tangoParserBean, timeBaseBean,
                wheelswitch, textField, checkBox, booleanComboBox, textArea, booleanTable, getStateLabel(),
                controlPanelButton);
    }

    /**
     * Updates the display.
     */
    private void updateVisibility() {
        updateControlPanelButtonVisibility();
        if (isAMotorClass()) {
            initMotorBean();
            motorBean.setVisible(true);
        } else if (isATimeBaseClass()) {
            initTimeBaseBean();
            timeBaseBean.setVisible(true);
            getStateLabel().setVisible(true);
        } else if (isATangoParserClass()) {
            initTangoParserBean();
            tangoParserBean.setVisible(true);
            getStateLabel().setVisible(true);
        } else {
            getStateLabel().setVisible(true);
            int attributeType = getAttributeType();
            int attributeFormat = TangoAttributeHelper.getAttributeFormat(model, attributeName);
            boolean isSettable = !TangoAttributeHelper.isAttributeReadOnly(model, attributeName);
            switch (attributeType) {
                case TangoConstHelper.SCALAR_TYPE:
                    switch (attributeFormat) {
                        case TangoConstHelper.STRING_FORMAT:
                            initReadNumberField();
                            readNumberField.setVisible(true);
                            if (isSettable) {
                                initTextField();
                            }
                            if (textField != null) {
                                textField.setVisible(isSettable);
                            }
                            break;
                        case TangoConstHelper.NUMERICAL_FORMAT:
                            initReadNumberField();
                            readNumberField.setVisible(true);
                            if (isSettable) {
                                initWheelSwitch();
                            }
                            if (wheelswitch != null) {
                                wheelswitch.setVisible(isSettable);
                            }
                            break;
                        case TangoConstHelper.BOOLEAN_FORMAT:
                            initCheckBox();
                            checkBox.setVisible(true);
                            if (isSettable) {
                                initBooleanComboBox();
                            }
                            if (booleanComboBox != null) {
                                booleanComboBox.setVisible(isSettable);
                            }
                            break;
                    }
                    break;
                case TangoConstHelper.ARRAY_TYPE:
                    if (attributeFormat == TangoConstHelper.STRING_FORMAT) {
                        initTextArea();
                        textArea.setVisible(true);
                    } else {
                        initChartViewer();
                        chartViewer.setVisible(true);
                        chartViewer.setPreferredSize(400, 400);
                    }
                    break;
                case TangoConstHelper.IMAGE_TYPE:
                    if (attributeFormat == TangoConstHelper.BOOLEAN_FORMAT) {
                        initBooleanTable();
                        booleanTable.setVisible(true);
                    } else {
                        initImageViewer();
                        imageViewer.setVisible(true);
                        imageViewer.setPreferredSize(400, 400);
                        // TODO REMOVE WHEN BUG 0025320 will be fixed
                        EDTManager.INSTANCE.runInDrawingThread(() -> {
                            imageViewer.setImageProperties(imageProperties);
                        });
                    }
                    break;

            }
        }
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            updateDefaultPanel();
            updateComplexPanel();
            revalidate();
            repaint();
        });
    }

    public void forceClearGUI() {
        setClassType(null);
        stringBox.disconnectWidgetFromAll(getStateLabel());
        stringBox.disconnectWidgetFromAll(readNumberField);
        stringBox.disconnectWidgetFromAll(textField);
        stringBox.disconnectWidgetFromAll(textArea);
        chartBox.disconnectWidgetFromAll(chartViewer);
        imageBox.disconnectWidgetFromAll(imageViewer);
        numberScalarBox.disconnectWidgetFromAll(wheelswitch);
        booleanMatrixBox.disconnectWidgetFromAll(booleanTable);
        booleanScalarBox.disconnectWidgetFromAll(checkBox);
        booleanScalarBox.disconnectWidgetFromAll(booleanComboBox);

        // TODO remove when JIRA JAVAAPI-137 will be fixed
        getStateLabel().setToolTipText(null);
        getStateLabel().setBackground(getBackground());
        if (readNumberField != null) {
            readNumberField.setToolTipText(null);
            readNumberField.setBackground(getBackground());
        }
        if (textField != null) {
            textField.setToolTipText(null);
            textField.setBackground(getBackground());
        }
    }

    @Override
    protected void clearGUI() {
        if (!modelIsSet) {
            forceClearGUI();
        }
    }

    @Override
    public void stop() {
        super.stop();
        if (motorBean != null) {
            motorBean.stop();
        }
        if (tangoParserBean != null) {
            tangoParserBean.stop();
        }
        if (timeBaseBean != null) {
            timeBaseBean.stop();
        }
    }

    @Override
    public void start() {
        if (isAMotorClass()) {
            initMotorBean();
            motorBean.start();
        } else if (isATangoParserClass()) {
            initTangoParserBean();
            tangoParserBean.start();
        } else if (isATimeBaseClass()) {
            initTimeBaseBean();
            timeBaseBean.start();
        }
        super.start();
    }

    @Override
    protected void refreshGUI() {
        hideAll();
        if (TangoDeviceHelper.isDeviceRunning(model)) {
            TangoKey attributeKey = null;
            if (attributeName != null) {
                attributeKey = generateAttributeKey(attributeName);
            }
            if (isATangoParserClass()) {
                initTangoParserBean();
                setStateModel();
                if (attributeKey != null) {
                    setScalarWidgetModel(attributeKey);
                }
            } else if (isATimeBaseClass()) {
                initTimeBaseBean();
                setStateModel();
                timeBaseBean.setModel(model);
            } else {
                setStateModel();
                int attributeType = getAttributeType();
                int attributeFormat = TangoAttributeHelper.getAttributeFormat(model, attributeName);
                switch (attributeType) {
                    case TangoConstHelper.SCALAR_TYPE:
                        setScalarWidgetModel(attributeKey);
                        break;
                    case TangoConstHelper.ARRAY_TYPE:
                        if (attributeFormat == TangoConstHelper.STRING_FORMAT) {
                            setStringSpectrumModel(attributeKey);
                        } else {
                            setChartWidgetModel(attributeKey);
                        }
                        break;
                    case TangoConstHelper.IMAGE_TYPE:
                        if (attributeFormat == TangoConstHelper.BOOLEAN_FORMAT) {
                            initBooleanTable();
                            booleanMatrixBox.connectWidget(booleanTable, attributeKey);
                        } else {
                            initImageViewer();
                            imageBox.connectWidget(imageViewer, attributeKey);
                        }
                        break;
                }
            }
            updateVisibility();
        }

    }

    private void setScalarWidgetModel(IKey attributeKey) {
        if (TangoAttributeHelper.isAttributeRunning(model, attributeName)) {
            boolean isSettable = !TangoAttributeHelper.isAttributeReadOnly(model, attributeName);
            int format = TangoAttributeHelper.getAttributeFormat(model, attributeName);

            if (format == TangoConstHelper.STRING_FORMAT) {
                initReadNumberField();
                stringBox.connectWidget(readNumberField, attributeKey);
                if (isSettable) {
                    initTextField();
                    IKey attributeWriteKey = generateWriteAttributeKey(attributeName);
                    stringBox.connectWidget(textField, attributeWriteKey);
                }
            } else if (format == TangoConstHelper.BOOLEAN_FORMAT) {
                initCheckBox();
                booleanScalarBox.connectWidget(checkBox, attributeKey);
                if (isSettable) {
                    initBooleanComboBox();
                    IKey attributeWriteKey = generateWriteAttributeKey(attributeName);
                    booleanScalarBox.connectWidget(booleanComboBox, attributeWriteKey);
                }
            } else if (format == TangoConstHelper.NUMERICAL_FORMAT) {
                initReadNumberField();
                stringBox.connectWidget(readNumberField, attributeKey);
                if (isSettable) {
                    initWheelSwitch();
                    IKey attributeWriteKey = generateWriteAttributeKey(attributeName);
                    numberScalarBox.connectWidget(wheelswitch, attributeWriteKey);
                }
            }
        }
    }

    private void setStringSpectrumModel(IKey attributeKey) {
        IDataSourceProducer producer = getProducer();
        if ((producer != null) && producer.isSourceCreatable(attributeKey)) {
            initTextArea();
            stringBox.connectWidget(textArea, attributeKey);
        }
    }

    private void setChartWidgetModel(IKey attributeKey) {
        if (attributeKey != null) {
            initChartViewer();
            chartBox.connectWidget(chartViewer, attributeKey);
            PlotProperties properties = plotPropertiesMap.get(attributeKey.getInformationKey().toLowerCase());
            if (properties != null) {
                if (properties.getAxisChoice() == -1) {
                    properties.setAxisChoice(IChartViewer.Y1);
                }
                chartViewer.setDataViewPlotProperties(attributeKey.getInformationKey(), properties);
            } else {
                chartViewer.setDataViewCometeColor(attributeKey.getInformationKey(), CometeColor.RED);
            }

            boolean isSettable = !TangoAttributeHelper.isAttributeReadOnly(model, attributeName);
            if (isSettable) {
                IKey writeAttributeKey = generateWriteAttributeKey(attributeName);
                chartBox.connectWidget(chartViewer, writeAttributeKey);
                properties = plotPropertiesMap.get(writeAttributeKey.getInformationKey().toLowerCase());
                if (properties != null) {
                    if (properties.getAxisChoice() == -1) {
                        properties.setAxisChoice(IChartViewer.Y1);
                    }
                    chartViewer.setDataViewPlotProperties(writeAttributeKey.getInformationKey(), properties);
                } else {
                    chartViewer.setDataViewCometeColor(writeAttributeKey.getInformationKey(), CometeColor.BLUE);
                }
            }
            if (chartProperties != null) {
                chartViewer.setChartProperties(chartProperties);
            } else {
                chartViewer.setChartProperties(defaultChartProperties);
            }
        }
    }

    public void setImageProperties(ImageProperties properties) {
        this.imageProperties = properties;
        if ((properties != null) && (imageViewer != null) && imageViewer.isVisible()) {
            imageViewer.setImageProperties(properties);
        }
    }

    public void setChartProperties(ChartProperties properties) {
        chartProperties = properties;
        if (chartViewer != null) {
            if ((properties != null) && chartViewer.isVisible()) {
                chartViewer.setChartProperties(properties);
            } else {
                chartViewer.setChartProperties(defaultChartProperties);
            }
        }
    }

    public void setPlotPropertiesMap(Map<String, PlotProperties> propertiesMap) {
        if (plotPropertiesMap != null) {
            plotPropertiesMap.clear();
            if (propertiesMap != null) {
                plotPropertiesMap.putAll(propertiesMap);
            }
            if ((chartViewer != null) && chartViewer.isVisible()) {
                Set<Entry<String, PlotProperties>> entrySet = plotPropertiesMap.entrySet();
                for (Entry<String, PlotProperties> dataView : entrySet) {
                    chartViewer.setDataViewPlotProperties(dataView.getKey(), dataView.getValue());
                }
            }
        }
    }

    public ImageProperties getImageProperties() {
        imageProperties = null;
        if ((imageViewer != null) && imageViewer.isVisible()) {
            imageProperties = imageViewer.getImageProperties();
        }
        return imageProperties;
    }

    public ChartProperties getChartProperties() {
        chartProperties = null;
        if ((chartViewer != null) && chartViewer.isVisible()) {
            chartProperties = chartViewer.getChartProperties();
        }
        return chartProperties;
    }

    public Map<String, PlotProperties> getPlotPropertiesMap() {
        plotPropertiesMap.clear();
        if ((chartViewer != null) && chartViewer.isVisible()) {
            DataViewList dataViews = chartViewer.getDataViewList();
            if ((dataViews != null) && !dataViews.isEmpty()) {
                List<DataView> dataViewList = dataViews.getListCopy();
                String dataViewId = null;
                for (DataView dataView : dataViewList) {
                    dataViewId = dataView.getId();
                    plotPropertiesMap.put(dataViewId, chartViewer.getDataViewPlotProperties(dataViewId));
                }
            }
        }
        return plotPropertiesMap;
    }

    @Override
    protected void onConnectionError() {
    }

    @Override
    protected void savePreferences(Preferences preferences) {
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
    }

    /**
     * Main method launches the application.
     * 
     * @param args
     */
    public static void main(final String... args) {
        int main = 1;
        if (args != null) {
            for (String arg : args) {
                try {
                    main = Integer.parseInt(arg);
                } catch (Exception e) {
                    main = 1;
                }
            }
        }
        switch (main) {
            case 2:
                main2();
                break;
            case 3:
                main3();
                break;
            case 1:
            default:
                main1();
                break;
        }
    }

    public static void main1() {
        String competeAttributeName = "ans-c01/dg/cpt.1";
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());

        final CommonDeviceAttributeBean bean = new CommonDeviceAttributeBean();
        panel.add(bean, BorderLayout.CENTER);

        final JComboBox<String> combo = new JComboBox<>();
        panel.add(combo, BorderLayout.SOUTH);
        combo.addItem("ans-c01/dg/cpt.1");
        combo.addItem("tango/tangotest/1/ampli");
        combo.addItem("tango/tangotest/1/short_scalar");
        combo.addItem("tango/tangotest/1/double_scalar");
        combo.addItem("tango/tangotest/titan/Double_Spectrum_ro");
        combo.addItem("tango/tangotest/titan/double_spectrum");
        combo.addItem("tango/tangotest/titan/wave");
        combo.addItem("tango/tangotest/titan/double_image");
        combo.addItem("i16-m-c06/op/mono1-mtp_fpitch.2/velocity");
        combo.addItem("FLO/TEST/PARSER.1/energy_w");
        combo.addItem("");
        combo.addActionListener((e) -> {
            bean.setCompleteAttributeName(combo.getSelectedItem().toString());
        });

        bean.setCompleteAttributeName(competeAttributeName);
        bean.setConfirmation(false);
        bean.setExecutedBatchFile("atkpanel");
        bean.start();

        JFrame frame = new JFrame();
        frame.setContentPane(panel);
        frame.pack();
        frame.setTitle(competeAttributeName);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public static void main2() {
        DataSourceProducerProvider.pushNewProducer(TangoDataSourceFactory.class);
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());

        final TextField bean = new TextField();

        final StringScalarBox scalarBox = new StringScalarBox();
        panel.add(bean, BorderLayout.CENTER);
        final JLabel focusLabel = new JLabel("grab Focus");
        focusLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                focusLabel.grabFocus();
            }
        });
        panel.add(focusLabel, BorderLayout.SOUTH);

        TangoKey attributeKey = new TangoKey();
        TangoKeyTool.registerAttribute(attributeKey, "tango/tangotest/titan", "double_scalar");
        scalarBox.connectWidget(bean, attributeKey);
        scalarBox.disconnectWidgetFromAll(bean);
        TangoKeyTool.registerAttribute(attributeKey, "katy/gs/publisher", "Double_Value");
        scalarBox.connectWidget(bean, attributeKey);
        scalarBox.disconnectWidgetFromAll(bean);

        JFrame frame = new JFrame();
        frame.setContentPane(panel);
        frame.pack();
        frame.setTitle("Test");
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public static void main3() {
        DataSourceProducerProvider.pushNewProducer(TangoDataSourceFactory.class);
        final NI6602 timeBaseBean = new NI6602();
        timeBaseBean.setModel("ans-c01/dg/cpt.1");
        timeBaseBean.start();

        JFrame frame = new JFrame();
        frame.setContentPane(timeBaseBean);
        frame.pack();
        frame.setTitle("Test NI6602");
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
